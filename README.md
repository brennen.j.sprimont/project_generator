Project Generator
=================

Generates projects based on templates and performs some string replacements.

For usage, run `python ./generate_project.py`

Example: `python ./generate_project.py --template python-cli --name my_new_project`


