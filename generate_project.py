#!/usr/bin/env python
"""!
Generates projects based on templates and does a bunch of fancy string replacements.
Run: `python ./generate_project.py for more details`

Project Template Directory:
./resources/templates/projects

@example
    ./generate_project.py --template python-cli --output-directory ~/projects --name my_new_project

Note: This project is written to be a simple script, the design and functions are simple. For
examples of projects using more complex designs see my other projects.
"""

import sys
import os
import shutil
import errno
import argparse
import pathlib
import json
import logging
import re
from typing import List

logger = logging.getLogger()

# All resources will be access relative to this file.
THIS_SCRIPT_PATH = os.path.realpath(__file__)
THIS_SCRIPT_DIRECTORY = os.path.dirname(THIS_SCRIPT_PATH)

# Directory containing project templates used to generate projects.
TEMPLATE_DIRECTORY = os.path.join(THIS_SCRIPT_DIRECTORY, "resources", "templates", "projects")

# Name of the file that identifies a directory as a project template.
TEMPLATE_IDENTIFIER_FILE = "__project_template__"


def find_template_directory(template_identifier: str) -> str:
    """!
    Finds the project template directory to copy based on the template identifier.

    @param template_identifier Identifier used to find the template directory. See the usage
       of the script for a complete list of options.
    @return The path to the specified project template. If it doesn't exist, a
        GenerateProjectException is raised.
    """
    template_components = template_identifier.split('-')
    template_directory_path = os.path.join(TEMPLATE_DIRECTORY, *template_components)
    logger.debug(f"Template Directory: {template_directory_path}")
    if os.path.exists(template_directory_path) == False:
        raise GenerateProjectException(f"Error! Template doesn't exist!   "
                                       f"Template Identifier: {template_identifier}")
    return template_directory_path


def generate_template_list(templates_directory: str) -> List[str]:
    """!
    Scans the template directory recursively looking for templates.
    A template is found once a "README.md" file is found.
    @param templates_directory   Directory to look for templates in.
    @return A list of template identifiers to generate templates for.
    """
    if os.path.exists(templates_directory) == False:
        raise GenerateProjectException(f"Error! Directory to look for templates in doesn't exist!   "
                                       f"Templates Directory: {templates_directory}")
    template_identifiers = []
    for root, directories, files in os.walk(templates_directory):
        for file_name in files:
            if file_name == TEMPLATE_IDENTIFIER_FILE:
                file_path = os.path.join(root, file_name)
                file_path = os.path.dirname(file_path.replace(templates_directory, ""))
                template_identifier = file_path.replace("/", "-").replace("\\", "-")[1:]
                template_identifiers.append(template_identifier)
    return template_identifiers


def copy_project_template(template_source_directory: str,
                          output_directory: str,
                          project_name: str) -> str:
    """!
    Copies the specified project template to the specified output directory with the specified name.

    @param template_source_directory Directory of the project template to copy.
    @param output_directory          Directory to place the copied project template.
    @param project_name              Name of the new project.
    @return The absolute path of the new project.
    """
    project_absolute_path = os.path.join(os.path.abspath(output_directory), project_name)
    logger.info(f"Generated project output path: {project_absolute_path}")
    if os.path.exists(project_absolute_path) == True:
        raise GenerateProjectException("Error! Output directory already exists! "
                                       "Choose a directory path that doesn't exist.")
    else:
        shutil.copytree(src=template_source_directory, dst=project_absolute_path)
        return project_absolute_path


def rename_directories(directory: str, replace_dictionary: dict):
    """!
    Renames all directories recursively from the specified directory.
    @param directory            The directory to rename directories in.
    @param replace_dictionary   Dictionary of string replacements.
    """
    for root, directories, files in os.walk(directory):
        for sub_directory in directories:
            for key, value in replace_dictionary.items():
                if key in sub_directory:
                    sub_directory_full_path = os.path.join(root, sub_directory)
                    new_directory_path = sub_directory_full_path.replace(key, value)
                    logger.debug(f"Renamed directory:   Old: {sub_directory_full_path}   New: {new_directory_path}")
                    os.rename(sub_directory_full_path, new_directory_path)


def rename_files(directory: str, replace_dictionary: dict):
    """!
    Renames all files recursively from the specified directory.
    @param directory            The directory to rename files in.
    @param replace_dictionary   Dictionary of string replacements.
    """
    for root, directories, files in os.walk(directory):
        for file_name in files:
            for key, value in replace_dictionary.items():
                if key in file_name:
                    file_path = os.path.join(root, file_name)
                    new_file_path = file_path.replace(key, value)
                    logger.debug(f"Renamed file:   Old: {file_path}   New: {new_file_path}")
                    os.rename(file_path, new_file_path)


def replace_keywords(directory: str, replace_dictionary: dict):
    """!
    Replaces all keywords in all files in the specified directory and all sub directories
    recursively.
    @param directory           The directory to replace keywords in.
    @param replace_dictionary  The dictionary containing the keywords and values to replace.
    """
    logger.info(f"Replacing keywords. Dictionary: {str(replace_dictionary)}")
    for root, directories, files in os.walk(directory):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            replacement_occurred = False
            with open(file_path, "r+") as current_file:
                file_string = current_file.read()
                for key, value in replace_dictionary.items():
                    if key in file_string:
                        replacement_occurred = True
                        file_string = file_string.replace(key, value)
                        logger.debug(f"Found keyword replacement: {key}   in file: {file_name}")
                if replacement_occurred is True:
                    current_file.seek(0)
                    current_file.truncate()
                    current_file.write(file_string)


def remove_file(directory: str, file_name: str):
    """!
    Removes the specified file from the directory.
    @param directory    Name of the directory to remove the file from.
    @param file_name    Name of the file to remove.
    """
    file_path = os.path.join(directory, file_name)
    if os.path.exists(file_path):
        logger.info(f"Removing file: {file_name}")
        os.remove(file_path)


def configure_logger():
    """!
    Configures the logger the script uses. For small scripts, setting up the "basicConfig" to use stdout
    is common. For larger projects, using rotating files and multiple named loggers is advised.
    """
    logging.basicConfig(format="%(asctime)s|%(levelname)s|%(filename)s:%(lineno)s|%(funcName)s|%(message)s",
                        stream=sys.stdout, level=logging.DEBUG)


def parse_arguments(template_identifiers: List[str]) -> dict:
    """!
    Parses the arguments passed into the command line interface. Python's argparse
    has built in support for -h and --help. Returns a dictionary containing the
    arguments passed in.

    @return A dictionary containing the arguments passed into command line interface
        paired with the argument name.
    """
    argument_parser = argparse.ArgumentParser(
        prog="generate_project.py",
        description="Generates projects based on templates.",
        usage="python generate_project.py --template python-cli --name my_new_project")
    argument_parser.add_argument("-o", "--output-directory",
                                 default="./",
                                 help="Directory to create the new project in. Default: %(default)s)")
    argument_parser.add_argument("-n", "--name",
                                 default="my_new_project",
                                 help="Name of the new project. Default: %(default)s)")
    argument_parser.add_argument("-t", "--template",
                                 choices=template_identifiers,
                                 default="python-generic",
                                 help="Project template to generate. Default: %(default)s)")
    arguments = argument_parser.parse_args()
    logger.info(f"Arguments: {vars(arguments)}")
    return vars(arguments)


def generate_project(output_directory: str,
                     name: str,
                     template: str):
    """!
    Generates a project based on the specified template.

    @param output_directory     Directory to output the generated project to.
    @param name                 Name of the new generated project.
    @param template             Template to generate the project from.
    """
    try:
        template_source_directory = find_template_directory(template)
        new_project_path = copy_project_template(template_source_directory,
                                                 output_directory,
                                                 name)
        replace_dictionary = {"__project_name__": name}
        rename_directories(new_project_path, replace_dictionary)
        rename_files(new_project_path, replace_dictionary)
        replace_keywords(new_project_path, replace_dictionary)
        remove_file(new_project_path, TEMPLATE_IDENTIFIER_FILE)
    except GenerateProjectException as exception:
        logger.error(exception)
        print(exception)


class GenerateProjectException(Exception):
    pass


if __name__ == "__main__":
    configure_logger()
    logger.info("Starting project generator...")
    template_identifiers = generate_template_list(TEMPLATE_DIRECTORY)
    arguments = parse_arguments(template_identifiers)
    generate_project(arguments["output_directory"], arguments["name"], arguments["template"])
    logger.info("Ending project generator...")
