#!/usr/bin/env python
"""!
TODO: perform a minimal overview of the project here. Write an indepth overview of the project in the README.md file.
"""

import sys
import os
import argparse
import logging
from logging.handlers import RotatingFileHandler

PROJECT_ROOT_DIRECTORY = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))

# Access the global singleton logger.
logger = logging.getLogger()


def configure_basic_logger():
    """!
    Configures a simple logger to stdout. For use in small scripts.
    """
    logging.basicConfig(format="%(asctime)s|%(levelname)s|%(filename)s:%(lineno)s|%(funcName)s|%(message)s",
                        stream=sys.stdout, level=logging.DEBUG)


def configure_complex_logger():
    """!
    Configures a rolling file logger. For use in large projects and production environments.
    """
    log_directory = os.path.join(PROJECT_ROOT_DIRECTORY, "logs")
    log_file_path = os.path.join(log_directory, "__project__name__.log")
    os.makedirs(log_directory, exist_ok=True)
    handler = RotatingFileHandler(log_file_path, maxBytes=20000, backupCount=10)
    formatter = logging.Formatter("%(asctime)s|%(levelname)s|%(filename)s:%(lineno)s|%(funcName)s|%(message)s")
    handler.setFormatter(formatter)
    handler.setLevel(logging.INFO)
    logger = logging.getLogger()
    logger.addHandler(handler)


def parse_arguments() -> dict:
    """!
    Parses the arguments passed into the command line interface. Python's argparse
    has built in support for -h and --help.

    @return A dictionary containing the arguments passed into command line interface
        paired with the argument name.
    """
    argument_parser = argparse.ArgumentParser(
        prog="generate_project.py",
        description="Prints 'Hello World!' in one of several languages. TODO: describe the program here.",
        usage="python __project_name__.py --language english")
    argument_parser.add_argument("-l", "--language",
                                 choices=["english", "spanish", "french"],
                                 default="english",
                                 help="What language to print hello world in. Default: %(default)s)")
    arguments = argument_parser.parse_args()
    logger.info(f"Arguments: {vars(arguments)}")
    return vars(arguments)


def __project_name__(language: str):
    if language == "english":
        print("Hello World!")
    elif language == "spanish":
        print("Hola Mundo!")
    elif language == "french":
        print("Bonjour le monde!")


if __name__ == "__main__":
    configure_basic_logger()
    configure_complex_logger()
    arguments = parse_arguments()
    logger.info("Starting __project_name__...")
    __project_name__(arguments.get("language"))
    logger.info("Stopping __project_name__...")
