__project_name__
==============

Overview
--------
TODO: Give an overview of the project.


Usage
-----
TODO: Describe how to use the application or interface with it if it is a library.


Terms
-----
TODO: Describe any domain specific business logic or terminology that a user may need to know to use/interface with this application.


Technologies
------------
 * python
 * pytest
 * doxygen
TODO: List the other technologies involved in the project (examples: sqlite, postgresql, docker, numpy, etc).


Logging Management
------------------
TODO: List file paths and directories to look for logs in.
TODO: Describe how to modify the logging filter (debug, info, warning, etc.).
