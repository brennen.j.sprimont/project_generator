#!/usr/bin/env python
"""!
Generates documentation using doxygen by scraping certain tokens
in the code and displays it in your browser.
"""

import os
import subprocess
import webbrowser

PROJECT_ROOT_DIRECTORY = os.path.join(os.path.dirname(__file__), "..")

# Move to the root of the project directory.
os.chdir(PROJECT_ROOT_DIRECTORY)
# Run doxygen, by default it will grab the "Doxyfile".
subprocess.check_call(["doxygen"])
# Open the generated documentation in the default web browser.
doxygen_index_path = os.path.join(PROJECT_ROOT_DIRECTORY, "documentation", "doxygen", "html", "index.html")
webbrowser.open(doxygen_index_path)
